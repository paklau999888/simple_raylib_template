
## About

This is a simple [Raylib](https://www.raylib.com/) boilerplate that I use as a starting point for my projects.

It takes care of the main loop, config file and build system.

## Structure

Header | Type | Global variable | Purpose
------ | ---- | --------------- | -------
`src/gameapp.h` | `Game_t` | `game` | State of the playfield.
. | `App_t` | `app` | Everything unrelated to current game state. (e.g. options, networking)
. | `Client_t` | `client` | Assets, graphics, user interface. Not present in a dedicated server build.
`src/config.h` | `AppConfig_t` | `app.config` | Fields to be stored in options.txt

Get started by extending the code at `src/gameapp.c`.

Don't forget to put your new source files in `src/CMakeLists.txt`.

## Building

### Visual Studio Code

* Install "CMake Tools" extension.
* Open this directory
* Press F7 to build, Shift+F5 to build and run, or F5 to debug.
* Ctrl+Shift+P -> "CMake: Select Variant" to choose between debug and release profile.

### CMake

```
mkdir build && cd build
cmake .. [optionally: -D DEDICATED_SERVER=1]
make
```

### Web (WASM)

* Requires [Emscripten](https://emscripten.org/) (and Ninja?)
* Run `misc\build_web.bat`
* This should produce `build_web\src\mygame.wasm.js` and `.wasm`
* Put it in on your web server together with `misc\index.html`
