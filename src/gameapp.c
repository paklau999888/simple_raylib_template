#include "gameapp.h"
#include "raymath.h"
#include "stdbool.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
#endif


AppState_t app = {
	.configPath = "options.txt"
};
GameState_t game;

#ifndef DEDICATED_SERVER
	ClientState_t client = {0};
#endif

void LoadAssets(){
	#ifndef DEDICATED_SERVER
		// ...
		// put your asset loading code here
		// ...
	#endif
}

void GameTick(){
	if(app.shouldRestartRound){
		ResetGame();
		app.shouldRestartRound = 0;
	}
	// ...
	// update game logic here
	// e.g.: UpdateCharacters();
	// 
	// runs at a fixed tick rate, don't use delta time!
	// ...
	game.frame++;
}

void DrawGraphics(){
	#ifndef DEDICATED_SERVER
		client.width = GetScreenWidth();
		client.height = GetScreenHeight();
		client.halfWidth = client.width / 2;
		client.halfHeight = client.height / 2;
		client.cam.offset.x = (float)client.halfWidth;
		client.cam.offset.y = (float)client.halfHeight;
		client.scale = client.dpi;
		client.time = (float)GetTime();
		client.deltaTime = GetFrameTime();

		switch(client.screen){
			case SCREEN_MAINMENU: {
				// you can replace this with your main menu screen
				StartGame();
				client.screen = SCREEN_GAME;
			} break;
			case SCREEN_GAME: {
				BeginDrawing();
				ClearBackground(RAYWHITE);
				BeginMode2D(client.cam);
					// ...
					// draw game graphics here
					// e.g.: DrawCharacters();
					// ...
				EndMode2D();
				DrawText("hello world", 150, 100, 24, BLACK);
				EndDrawing();
			} break;
		}
	#endif
}

void StartGame(){
	memset(&game, 0, sizeof(game));
	//...
	// pass initial state here (game mode, networking role, etc.)
	//...
	ResetGame();
}

void ResetGame(){
	game.state = STATE_UNINITIALIZED;
	game.frame = 0;
	game.timer = 0;

	if(!game.initialized){
		game.initialized = 1;
	}else{
		CloseGame();
		app.gamesPlayed++;
	}

	#ifndef DEDICATED_SERVER
		client.cam = (Camera2D){
			.target = {0, 0},
			.rotation = 0,
			.zoom = 1
		};
		//...
		// reset client UI state here
		//...
	#endif
}

void CloseGame(){
	//...
	// clean up the game board
	//...
}
